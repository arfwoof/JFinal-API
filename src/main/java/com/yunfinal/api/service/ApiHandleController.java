package com.yunfinal.api.service;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

/**
 * 接口参数交互， 统一规范化自动处理
 * 杜福忠 2019-02-18
 */
@Before(ApiHandleInterceptor.class)
public abstract class ApiHandleController extends Controller {
    protected static ApiCheck kit = ApiCheck.ME;

    public abstract Object index(JSONObject kv);

    /**
     * 错误直接返回
     *
     * @param code
     * @param msg
     */
    protected void returnError(String code, String msg) {
        throw new ApiException(code, msg);
    }

    /**
     * 错误直接返回
     *
     * @param code
     * @param msg
     * @param data
     */
    protected void returnError(String code, String msg, Object data) {
        throw new ApiException(code, msg, data);
    }


}
