package com.yunfinal.api.service.db;

import com.alibaba.fastjson.JSONObject;

public class DbWhere extends JSONObject {

    public DbWhere() {}

    /**
     *
     * @param k 字段
     * @param v 值
     */
    public DbWhere(String k, String v) {
        put("k", k);
        put("v", v);
    }

    /**
     * @param w and 或者 or
     * @param k 字段
     * @param x 条件
     * @param v 值
     */
    public DbWhere(String w, String k, String x, String v) {
        put("k", k);
        put("v", v);
        put("w", w);
        put("x", x);
    }

    public String getW() {
        return getString("w");
    }

    public void setW(String w) {
        put("w", w);
    }

    public String getK() {
        return getString("k");
    }

    public void setK(String k) {
        put("k", k);
    }

    public String getX() {
        return getString("x");
    }

    public void setX(String x) {
        put("x", x);
    }

    public String getV() {
        return getString("v");
    }

    public void setV(String v) {
        put("v", v);
    }
}
