
#### 说明
JFinal-API 开发的极简美学！

非常适合前后分离式开发，内置交互数据格式JSON，自带简洁生成API文档.MD工具，
junit测试工具类, 服务报告单实时监控等...


jfinal-undertow 下开发 ， JAVA 1.8+

运行
StartUndertowServerApp.java
即可看见效果

![输入图片说明](https://images.gitee.com/uploads/images/2019/0309/010541_80de59e8_619050.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0309/010245_5fb1f631_619050.png "屏幕截图.png")

内置Demo：
src/main/java/com/yunfinal/api/demo
 _不一样的开发体验_ 

```
/**
 * 自定义控制器 API 接口
 */
public class DemoApi extends ApiHandleController {

    @ApiDoc("接口编写的例子")
    @Override
    public Object index(JSONObject kv) {
        return "你好， 试试err";
    }

    @ApiDoc("接口收发json的例子")
    public Object json(JSONObject kv) {
        return kv;
    }

    @ApiDoc("接口验证参数的例子")
    public Object error(JSONObject kv) {
        return kit.get(kv, "name");
    }

    @ApiDoc("接口参数转换Record对象的例子")
    public Object toRecord(JSONObject kv) {
        //new Record().setColumns(kv.getJSONObject("record"));
        Record record = new Record().setColumns(kv);
        return record;
    }

    @ApiDoc("接口自定义非法异常的例子")
    public Object exception(JSONObject kv) {
        throw new ApiException("500", "再试试也是错误的", "exception");
    }

    @ApiDoc("接口数据库操作的例子")
    public Object db(JSONObject kv) {
        if (!"1234567".equals(kv.getString("token"))) {
            returnError("403", "令牌已过期", "token");
        }
        if (null == Db.use()) {
            returnError("405", "数据库连接池未启动", "main");
        }
        return DbService.srvDb.index(kv);
    }

}
```


夸张的JSON直操作数据库, 可接管白名单表，急速出接口就靠它了，一般得搭配权限认证业务
com.yunfinal.api.service.db.DbService.java
```
    /**
     * <pre>
     * {
     * configName : "main", //非必填
     * type : "find | save | delete | update", //非必填
     * tableName : "", // * 必填
     * primaryKey : "id", //非必填
     * data : {
     *      column: ""
     *      }
     * }
     * </pre>
     */
    public Object index(JSONObject kv) {...}
```


写接口用的项目一般都不想写Model和维护它，Db + Record模式肯定是必备了，但是你又怕项目中SQL乱窜，SQL管理与动态生成是必备了，项目里薄薄的封装了一下SQL模版的“高级用法”：https://www.jfinal.com/doc/5-13
 _com.yunfinal.api.service.db.dao.*_ 
```
 * 使用Jfinal 的SQL模版引擎，对SqlPara的参数map进行封装，方便API开发使用，
 * ActiveRecordPlugin 中记得放入配合模版
 * arp.addSqlTemplate("/sql/common.sql");
 * <p>
 * 例子
 * <pre>
 * public Object getCourseOrdersDetail(JSONObject kv) {
 * //获取购课记录的时间，编号等信息
 * FindKv findKv = FindKv.bySelect("id, ordernum, teachtype, createtime, subjectids");
 * //条件 UserKit是自建的工具
 * findKv.setCond(CondKv.by("studentid=", kv.get("id")));
 * //排序 倒序
 * findKv.setOrderBy("createtime DESC");
 * //分页
 * findKv.setLimit(kv);
 * 继承BaseDao的子类（GenerateDao生成器生成）或FindKvKit
 * List<Record> list = FindKvKit.find(findKv);
 * for (Record r : list) {
 * handerOrder(r);
 * }
 * return orderList;
 * }
 * </pre>
```
还有些小技巧都在代码中，这个API工具是我们公司对原有项目不想继续在原项目代码中写了， 就独立出来，方便维护和使用新技术，用同一个数据库，做成独立接口项目后，抽取出来的。尽量干净，易懂，易维护！刚入行的妹子都能轻易上手写出漂亮的接口代码哦~


JavaDoc 文档：
https://apidoc.gitee.com/bean80/JFinal-API

jfinal-undertow学习资料：
https://www.jfinal.com/doc/1-5

_更多API开发爽体验，试用后就知道了~_
#### 运行应用

打包

```
mvn clean package -Dmaven.test.skip=true
```

运行

```
Windows电脑运行地址 target/JFinal-API-release/JFinal-API/start.bat
Linux和Mac 运行地址 target/JFinal-API-release/JFinal-API/start.sh

点击控制台的 http://127.0.0.1:8080
或者在浏览器输入 http://127.0.0.1:8080 即可访问
```
